package Ai;

import java.util.ArrayList;
import java.util.Objects;

//Denne klasse bruges til AI og er forskellig fra tile, da vores ai ikke skal have adgang til den tå data under.
public class Celle {
    byte x;
    byte y;

    boolean confirmedBom = false;
    boolean shown = false;

    ArrayList<Par> par;

    public Celle(byte x, byte y){
        this.x = x;
        this.y = y;

        par = new ArrayList<>();
    }

    public String getID(){
        return x + "," +y;
    }

    //denne er lavet da det eneste vigtige for at tjekke om to klasser er det samme er om de har samme kordinator.
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Celle celle = (Celle) o;
        return x == celle.x &&
                y == celle.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    public void addPar(Par par){
        this.par.add(par);
    }


}
