package Ai;

import MineSweeper.Models.Board;
import MineSweeper.Models.CoordCode;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Ai {
    Celle[][] cells;
    int xSize;
    int ySize;
    ArrayList<Par> par;

    public boolean solveAll(Board board){
        LinkedList<Integer> hash = new LinkedList<>();

        //Så længe spillet ikke er vundet, forsøg at løse det. Hvis der 5 gange ikke er sket en ændring, så stop.
        while (!board.isGameWon()){
            oneStep(true,board);
            int temp = par.hashCode();
            if(hash.size() > 5){
                if(temp == hash.removeFirst() && temp == hash.get(4)){
                    return false;
                }
            }
            hash.add(temp);
        }
        return true;
    }

    //Forsøg at finde et enkelt svar, tre gange.
    public CoordCode oneStep(){
        for(int i = 0; i < 3; i++){
            CoordCode temp = oneStep(false,null);
            if(temp != null){
                return temp;
            }
        }
        return null;
    }

    public CoordCode oneStep(boolean solveAll, Board board){
        //Hvis antal bomber og antal felter er det samme. Så er alle felterne bpmber
        //defor fjernes felterne fra alle par. Og fra alle par fjernes der bomber tilsvarende til delte felter.
        ArrayList<Par> toBeRemoved = new ArrayList<>();
        for (Par current : par) {
            if(current.getDiff() == 0){
                for(Celle celle : current.celler){
                    celle.confirmedBom = true;
                    celle.par.remove(current);
                    for(Par sub: celle.par){
                        if(sub.celler.contains(celle) && sub != current) {
                            sub.count -= 1;
                            sub.celler.remove(celle);
                        }
                    }
                }
                toBeRemoved.add(current);
            }
        }
        par.removeAll(toBeRemoved);
        toBeRemoved.clear();

        ArrayList<CoordCode> coordCodesToAnalyse = new ArrayList<>();
        //Hvis der er 0 bomber må alle felter være åbnne. Hvis der kun skal findes en bliver den første celle i paret sat som
        //åben og fjernet fra lle par, der efter bliver denne retuneret. HVis der skal findes så mange som muligt gennemløbes
        //aller celler i paret og det samme gøres ved dem, med undtagelse af at de bliver retuneret.
        for(Par current: par){
            if(current.celler.size() !=0) {
                if (current.count == 0) {
                    if(solveAll) {
                        for (Celle celle : current.celler) {
                            for (Par sub : celle.par) {
                                if (sub != current) {
                                    sub.celler.remove(celle);
                                }
                            }
                            celle.shown = true;
                            coordCodesToAnalyse.addAll(board.showTiles(celle.getID()));
                            celle.par.remove(current);
                        }
                        toBeRemoved.add(current);
                    }else{
                        Celle temp = current.celler.get(0);
                        temp.shown=true;
                        for(Par sub : temp.par){
                            sub.celler.remove(temp);
                        }

                        return cellToCoordCode(temp);

                    }
                }
            }else {
                toBeRemoved.add(current);
            }
        }

        par.removeAll(toBeRemoved);

        if(solveAll){
            createPairs(coordCodesToAnalyse);
            coordCodesToAnalyse.clear();
        }

        ArrayList<Par> toBeAdded = new ArrayList<>();

        //Gå igennem alle celler og forsøg at trække dem fra par de deler celler med. Hvis dette lykkes tjekkes det
        //om paret allerede eksistre, hvis ikke tilføjes det.
        for(Par current : par){
            for(Celle celle : current.celler){
                for(Par sub : celle.par){
                    if(sub != current){
                        Par ny = subtractPairs(sub, current);
                        if(ny != null && !checkIfExits(ny)){
                            toBeAdded.add(ny);
                            for(Celle cel : ny.celler){
                                cel.par.add(ny);
                            }
                        }
                    }
                }
            }
        }
        par.addAll(toBeAdded);
        return null;
    }

    private CoordCode cellToCoordCode(Celle celle){
        return new CoordCode(celle.x,celle.y,-2);
    }

    public Ai(int gridX, int gridY) {
        xSize = gridX;
        ySize = gridY;

        par = new ArrayList<>();

        //Opret en liste af alle celler
        cells = new Celle[gridX][gridY];
        for(byte i = 0; i < xSize; i++){
            for(byte j = 0; j < ySize; j++){
                cells[i][j] =new Celle(i,j);
            }
        }
    }

    public boolean checkIfExits(Par par){
        for(Celle celle : par.celler){
            if(celle.par.contains(par)){
                return true;
            }
        }
        return false;
    }

    //Denne metode forsøger at trække to par fra hindanen i overenstemmelse med hvad der er beskrevet i raporten.
    //Hvis det ikke er muligt retuneres null
    public Par subtractPairs(Par par1, Par par2){
        Par re = new Par();
        re.celler = ((ArrayList<Celle>)par1.celler.clone());

        //count = antal celler der ikke er tilfældes
        int count = 0;
        for(Celle current : par2.celler){
            if(!re.celler.remove(current)){
                count++;
            }
        }

        //Hvis forskellen mellem antal bomber er den samme som antallet af felter der ikke er
        //tilfælles for de to par, så lav et nyt par, med dem som er til fælles, med antal bomber = 0
        if(par2.count > par1.count && count == par2.count - par1.count && par2.count > 1 && par1.celler.size()-count >=2){
            re.count = 0;
        }else if(count != 0 || re.celler.size() == 0){
            re = null;
        }else{
            re.count = (byte) ( par1.count - par2.count);
        }

        return re;
    }

    //Denne metode tager en liste af kordinator. Listen skal repræcentere de celler der lige er blevet vist.
    //Den laver den der efter til par og ligger dem ind i listen.
    public void createPairs(List<CoordCode> in){
        ArrayList<Par> re = new ArrayList<>();

        //Fjern de celler der nu er vist fra alle par.
        for(CoordCode current: in){
            Celle celle = cells[current.x][current.y];
            celle.shown = true;
            for(Par sub : celle.par){
                sub.celler.remove(celle);
            }
        }

        //Opret par hvor alle omkring liggende celler bliver tilføjet til.
        for(CoordCode current: in){
            if(current.num > 0){
                Par temp = new Par();

                temp.count = (byte)current.num;
                temp.celler = new ArrayList<>();
                for(int i = Math.max(current.x-1,0); i <= Math.min(current.x+1,xSize-1);i++){
                    for(int j = Math.max(current.y-1,0); j <= Math.min(current.y+1,ySize-1);j++){
                        if(!cells[i][j].shown){
                            if(cells[i][j].confirmedBom){
                                temp.count--;
                            }else {
                                temp.celler.add(cells[i][j]);
                                cells[i][j].par.add(temp);
                            }
                        }
                    }
                }
                re.add(temp);
            }
        }
        par.addAll(re);
    }
}
