package Ai;

import java.util.ArrayList;
import java.util.Objects;

public class Par {
    byte count;
    ArrayList<Celle> celler;

    int getDiff(){
        return celler.size()-count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Par par = (Par) o;
        return count == par.count &&
                Objects.equals(celler, par.celler);
    }

    @Override
    public int hashCode() {
        return Objects.hash(count, celler);
    }
}
