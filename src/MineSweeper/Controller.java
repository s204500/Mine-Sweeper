package MineSweeper;

import Ai.Ai;
import MineSweeper.Models.Board;
import MineSweeper.Models.CoordCode;
import MineSweeper.Models.Tile;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.awt.*;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

public class Controller {
    // Tile event handlers
    private static final EventHandler<MouseEvent> enterHandler = e -> {
        // Change tile color to hover color
        StackPane stackPane = (StackPane)e.getSource();
        Rectangle rectangle = (Rectangle)stackPane.getChildren().get(0);
        rectangle.setFill(GuiHelper.hoverColor);
    };
    private static final EventHandler<MouseEvent> exitHandler = e -> {
        // Change tile color back to hidden color
        StackPane stackPane = (StackPane)e.getSource();
        Rectangle rectangle = (Rectangle)stackPane.getChildren().get(0);
        rectangle.setFill(GuiHelper.hiddenColor);
    };
    private final EventHandler<MouseEvent> clickHandler = new EventHandler<>() {
        @Override
        public void handle(MouseEvent e) {
            StackPane stackPane = (StackPane)e.getSource();

            // On left click, if board is not initialized, initialize board, time, and AI
            if (!board.initialized && e.getButton() == MouseButton.PRIMARY) {
                board.initialize(mines, stackPane.getId());
                initializeTime(System.currentTimeMillis());
                ai = new Ai(board.getXDim(), board.getYDim());
            }
            // On right click, if board is initialized, set or unset flag
            if (board.initialized && e.getButton() == MouseButton.SECONDARY) {
                setFlag(stackPane.getId(), board.setFlag(stackPane.getId()));
            }
            // On left click if board is initialized, reveal tile
            if (e.getButton() == MouseButton.PRIMARY) {
                unpackList(board.showTiles(stackPane.getId()));
            }
        }
    };

    // GameIndicator event handlers
    private static final EventHandler<MouseEvent> enterIndicatorHandler = e -> {
        // Change indicator color to hover color
        Circle indicator = (Circle)e.getSource();
        switch ((String)indicator.getUserData()) {
            case "lost":
                indicator.setFill(GuiHelper.lostStroke);
                break;
            case "running":
                indicator.setFill(GuiHelper.runningStroke);
                break;
            case "won":
                indicator.setFill(GuiHelper.wonStroke);
                break;
            default:
                break;
        }
    };
    private static final EventHandler<MouseEvent> exitIndicatorHandler = e -> {
        // Change indicator color to regular color
        Circle indicator = (Circle)e.getSource();
        switch ((String)indicator.getUserData()) {
            case "lost":
                indicator.setFill(GuiHelper.lostFill);
                break;
            case "running":
                indicator.setFill(GuiHelper.runningFill);
                break;
            case "won":
                indicator.setFill(GuiHelper.wonFill);
                break;
            default:
                break;
        }
    };

    private GuiHelper guiHelper;

    // Data fields
    private Board board;
    private int rows;
    private int columns;
    private int mines;
    private int counter;

    //Timer
    private long timeStart = 0;
    private long timeEnd = 0;
    private boolean gameDone = false;
    Timer timer;

    //Ai
    Ai ai;

    // FXML elements
    @FXML private RadioButton radioEasy;
    @FXML private RadioButton radioIntermediate;
    @FXML private RadioButton radioExpert;
    @FXML private RadioButton radioCustom;
    @FXML private TextField inputRows;
    @FXML private TextField inputColumns;
    @FXML private TextField inputMines;
    @FXML private Button buttonNewGame;
    @FXML private Button buttonSaveGame;
    @FXML private Button buttonLoadGame;
    @FXML private Circle gameIndicator;
    @FXML private GridPane gameGrid;
    @FXML private Text timerFelt;
    @FXML private Button buttonOneStepAI;
    @FXML private Button buttonAiGame;
    @FXML private Text counterFelt;

    // Controller initialization
    @FXML protected void initialize() {
        ToggleGroup radioGroup = radioCustom.getToggleGroup();
        radioGroup.selectedToggleProperty().addListener(((observableValue, oldToggle, newToggle) -> setGameSettingInputsDisabled()));

        setBoardDimensionInput(inputRows);
        setBoardDimensionInput(inputColumns);
        setMinesInput(inputMines);
        buttonNewGame.setOnAction(e -> createNewGame());
        buttonSaveGame.setOnAction(e -> saveGame());
        buttonLoadGame.setOnAction(e -> loadGame());

        GuiHelper.setRunning(gameIndicator);
        gameIndicator.setOnMouseEntered(enterIndicatorHandler);
        gameIndicator.setOnMouseExited(exitIndicatorHandler);
        gameIndicator.addEventFilter(MouseEvent.MOUSE_CLICKED, e-> createNewGame());

        loadGameSettings();

        timerFelt.setText("000");

        buttonOneStepAI.setOnAction(e -> aiHelp());

        buttonAiGame.setOnAction(e -> createAiBoard());

    }

    // Convert a set of Board coordinates to tile node ids
    public  String toId(int x, int y){
        return x+ "," +y;
    }

    // Set the view's stage
    public void setStage(Stage stage) {
        Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        double appHeight = screenSize.getHeight() * 0.9;
        double appWidth = appHeight - 150;
        guiHelper = new GuiHelper(appWidth);
        stage.setHeight(appHeight);
        stage.setWidth(appWidth);

        setBoard();
    }

    // Save game parameters
    private void saveGameSettings() {
        try {
            File file = new File("settings");
            file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.write(rows + " " + columns + " " + mines);
            writer.close();
        } catch (Exception e) {
            // Do nothing
        }
    }

    // Load game parameters
    private void loadGameSettings() {
        try {
            File settings = new File("settings");
            Scanner scanner = new Scanner(settings);
            rows = scanner.nextInt();
            columns = scanner.nextInt();
            mines = scanner.nextInt();
            scanner.close();
        } catch (Exception e) {
            setIntermediate();
        }

        getGameSettings();
    }

    // Get the current game settings from fields and set controls accordingly
    private void getGameSettings() {
        if (rows == 9 && columns == 9 && mines == 10) {
            radioEasy.setSelected(true);
        }
        else if (rows == 16 && columns == 16 && mines == 40) {
            radioIntermediate.setSelected(true);
        }
        else if (rows == 16 && columns == 30 && mines == 99) {
            radioExpert.setSelected(true);
        }
        else {
            radioCustom.setSelected(true);
            inputRows.setText(rows + "");
            inputColumns.setText(columns + "");
            inputMines.setText(mines + "");
        }
    }

    // Set the current game parameter fields based on selected control
    private void setGameSettings() {
        if (radioEasy.isSelected()) {
            setEasy();
        }
        else if (radioIntermediate.isSelected()) {
            setIntermediate();
        }
        else if (radioExpert.isSelected()) {
            setExpert();
        }
        else if (radioCustom.isSelected()){
            setCustom();
        }

        saveGameSettings();
    }

    // Set disabled attribute of custom game parameter inputs
    private void setGameSettingInputsDisabled() {
        boolean selected = radioCustom.isSelected();
        inputRows.setDisable(!selected);
        inputColumns.setDisable(!selected);
        inputMines.setDisable(!selected);
    }

    // Set a text field to accept only integer values
    private static void setNumericField(TextField input) {
        input.textProperty().addListener((observableValue, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                input.setText(oldValue);
            }
        });
    }

    // Set validation of mines input
    private void validateMinesInput() {
        try {
            int mines = Integer.parseInt(inputMines.getText());
            int rows = Integer.parseInt(inputRows.getText());
            int columns = Integer.parseInt(inputColumns.getText());
            mines = Math.max(1, mines);
            mines = Math.min(rows * columns - 9, mines);
            inputMines.setText(mines + "");
        } catch (NumberFormatException e) {
            inputMines.setText(1 + "");
        }
    }

    // Set handlers on board dimension input
    private void setBoardDimensionInput(TextField input){
        setNumericField(input);
        input.focusedProperty().addListener(observable -> {
            if (!input.isFocused()) {
                try {
                    int value = Integer.parseInt(input.getText());
                    value = Math.max(4, value);
                    value = Math.min(36, value);
                    input.setText(value + "");
                } catch (NumberFormatException e) {
                    input.setText(4 + "");
                }
                validateMinesInput();
            }
        });
    }

    // Set handlers on mines input
    private void setMinesInput(TextField input) {
        setNumericField(input);
        input.focusedProperty().addListener(observable -> {
            if (!input.isFocused()) {
                validateMinesInput();
            }
        });
    }

    // Set game settings to easy
    private void setEasy() {
        rows = 9;
        columns = 9;
        mines = 10;
    }

    // Set game settings to intermediate
    private void setIntermediate() {
        rows = 16;
        columns = 16;
        mines = 40;
    }

    // Set game settings to expert
    private void setExpert() {
        rows = 16;
        columns = 30;
        mines = 99;
    }

    // Set game settings to custom inputs
    private void setCustom() {
        try {
            rows = Integer.parseInt(inputRows.getText());
            columns = Integer.parseInt(inputColumns.getText());
            mines = Integer.parseInt(inputMines.getText());
        } catch (NumberFormatException e) {
            setIntermediate();
        }
    }

    // Create a new board and reset game
    private void createNewGame() {
        timeEnd = timeStart;
        gameDone =true;
        setGameSettings();
        setBoard();
        GuiHelper.setRunning(gameIndicator);
    }

    // Create a board solvable by the AI
    public void createAiBoard(){
        setGameSettings();
        setBoard();
        Board temp;
        while (true){
            temp= new Board(rows,columns);
            String first = toId((int)(Math.random()*rows),(int)(Math.random()*columns));
            temp.initialize(mines,first);


            ai = new Ai(rows,columns);
            List<CoordCode> firstTiels = temp.showTiles(first);
            ai.createPairs(firstTiels);
            try {
                if(ai.solveAll(temp.clone())){
                    board = temp;
                    ai = new Ai(rows,columns);
                    unpackList(firstTiels);
                    initializeTime(System.currentTimeMillis());
                    return;
                }
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }

        }

    }

    // Save the current game
    private void saveGame(){
         try{
             File file = new File("saveFile");
             file.createNewFile();
             FileWriter writer = new FileWriter(file);
             Tile[][] boardState = board.getBoardState();
             writer.write(boardState.length+","+boardState[0].length+","+(System.currentTimeMillis()-timeStart)+","+mines+"\n");
             for (Tile[] row : boardState){
                 for(Tile e : row){
                     writer.write((e.getState()?"1":"0")+","+e.getNum()+","+(e.getFlag()?"1":"0")+":");
                 }
                 writer.write(".");
             }
             writer.close();
             System.out.println("Game has been Saved");

         } catch (IOException e) {
             System.out.println("Error");
             e.printStackTrace();
         }
    }

    // Replace current game with the loaded game
    private void loadGame(){
        try {
            File file = new File("saveFile");
            Scanner reader = new Scanner(file);
            String[] args = reader.nextLine().split(",");

            rows = Integer.parseInt(args[0]);
            columns = Integer.parseInt(args[1]);
            mines = Integer.parseInt(args[3]);
            getGameSettings();

            createNewGame();
            ai = new Ai(board.getXDim(), board.getYDim());
            String data = reader.nextLine();
            unpackList(board.loadGame(data));
            setFlags();
            initializeTime(System.currentTimeMillis()-Integer.parseInt(args[2]));


        } catch (FileNotFoundException e){
            System.out.println("Error");
            e.printStackTrace();
        }
    }

    // Based on Board state, set all current flags
    private void setFlags() {
        Tile[][] tiles = board.getBoardState();
        for(int i=0; i< tiles.length; i++){
            for(int j=0; j< tiles[0].length; j++){
                if(tiles[i][j].getFlag()){
                    setFlag(i+","+j,true);
                }
            }
        }
    }

    // Use AI to make a move
    private void aiHelp(){
        if(board.initialized) {
            CoordCode temp = ai.oneStep();
            if (temp != null) {
                unpackList(board.showTiles(temp.toID()));
                timeStart -= 10000;
            }
        }
    }

    // Set new board with current parameters
    private void setBoard() {
        board = new Board(rows,columns);
        gameGrid.getChildren().clear();

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                StackPane stack = new StackPane();
                Rectangle tile = new Rectangle();
                tile.setWidth(guiHelper.tileSize);
                tile.setHeight(guiHelper.tileSize);
                tile.setFill(GuiHelper.hiddenColor);
                tile.setStrokeType(StrokeType.INSIDE);
                tile.setStrokeWidth(0.25);
                tile.setStroke(Color.BLACK);

                addEventFilters(stack);

                stack.setId(i + "," + j);
                stack.getChildren().add(tile);
                gameGrid.add(stack, j, i);
            }
        }
        counter = mines;
        counterFelt.setText(Math.max(0, counter) + "");
    }

    // Set handlers on tile
    private void addEventFilters(StackPane tile){
        tile.addEventFilter(MouseEvent.MOUSE_ENTERED, enterHandler);
        tile.addEventFilter(MouseEvent.MOUSE_EXITED, exitHandler);
        tile.addEventFilter(MouseEvent.MOUSE_CLICKED, clickHandler);
    }

    // Reveal the tiles on coordinates listed and check for game won
    private void unpackList(List<CoordCode> list){
        ai.createPairs(list);
        for (CoordCode e : list){
            revealTile(e.x+","+e.y,e.num);
        }
        if (board.isGameWon()){
            runSuccess();
        }
    }

    // Reveal a tile
    private void revealTile(String id, int resultCode) {
        StackPane stack = (StackPane) gameGrid.lookup("#"+id);
        Rectangle tile = getTile(stack);
        stack.removeEventFilter(MouseEvent.MOUSE_ENTERED, enterHandler);
        stack.removeEventFilter(MouseEvent.MOUSE_EXITED, exitHandler);
        stack.removeEventFilter(MouseEvent.MOUSE_CLICKED, clickHandler);

        if (tile != null) {
            if (resultCode < 0) {
                // If resultCode < 0, tile contained a mine, and game is lost
                runFailure(id);
            } else {
                tile.setFill(GuiHelper.revealedColor);

                if (resultCode > 0) {
                    // If resultCode > 0, tile should display the resultCode
                    Text tal = new Text(resultCode +"");
                    tal.setFont(guiHelper.numFont);

                    tal.setFill(GuiHelper.numColors[resultCode-1]);

                    stack.getChildren().add(tal);
                }
            }
        }
    }

    // Run game ending for winning
    private void runSuccess() {
        timeEnd = System.currentTimeMillis();
        gameDone = true;
        removeHandlers();

        GuiHelper.setWon(gameIndicator);

        List<String> mineIds = board.getMineIds();
        for (String id: mineIds) {
            setFlag(id, true);
        }

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                String id = i + "," + j;
                StackPane pane = (StackPane)gameGrid.lookup("#" + id);
                Rectangle tile = getTile(pane);

                GuiHelper.victoryFlash(tile, i + j, mineIds.contains(id));
            }
        }
    }

    // Run game ending for losing
    private void runFailure(String id) {
        timeEnd = System.currentTimeMillis();
        gameDone = true;
        removeHandlers();

        GuiHelper.setLost(gameIndicator);

        List<String> mineIds = board.getMineIds();
        mineIds.remove(id);
        StackPane stackPane = (StackPane)gameGrid.lookup("#" + id);
        Rectangle tile = getTile(stackPane);
        GuiHelper.explodeMine(tile, 0);

        Random random = new Random();
        int i = 0;
        while (mineIds.size() > 0) {
            i++;
            String randomId = mineIds.get(random.nextInt(mineIds.size()));
            mineIds.remove(randomId);
            stackPane = (StackPane)gameGrid.lookup("#" + randomId);
            tile = getTile(stackPane);
            GuiHelper.explodeMine(tile, i);
        }
    }

    // Remove handlers from all tiles
    private void removeHandlers(){
        List<Node> nodes = gameGrid.getChildren();
        for (Node tile: nodes) {
            tile.removeEventHandler(MouseEvent.MOUSE_ENTERED, enterHandler);
            tile.removeEventHandler(MouseEvent.MOUSE_EXITED, exitHandler);
            tile.removeEventHandler(MouseEvent.MOUSE_CLICKED, clickHandler);
            tile.removeEventFilter(MouseEvent.MOUSE_CLICKED, clickHandler);
            tile.removeEventFilter(MouseEvent.MOUSE_ENTERED, enterHandler);
            tile.removeEventFilter(MouseEvent.MOUSE_EXITED, exitHandler);
        }
    }

    // Set or unset flag on tile
    private void setFlag(String id, boolean set){
            StackPane stack = (StackPane) gameGrid.lookup("#" + id);
            Node childFlag = null;
            for (Node child : stack.getChildren()) {
                if (child instanceof Polygon) {
                    childFlag = child;
                    setCounter(true);
                    break;
                }
            }
            stack.getChildren().remove(childFlag);
            if (set) {
                Polygon flag = guiHelper.getFlag();
                stack.getChildren().add(flag);
                setCounter(false);
            }
    }

    // Increment or decrement counter
    private void setCounter(boolean shouldIncrement){
        if (shouldIncrement){
            counter++;
        }
        else {
            counter--;
        }

        counterFelt.setText("0".repeat((mines + "").length() - (Math.max(0, counter) + "").length()) + Math.max(0, counter));
    }

    // Get the Rectangle node of the containing StackPane or return null if no such node
    private Rectangle getTile(StackPane stackPane) {
        for (Node childNode: stackPane.getChildren()) {
            if (childNode instanceof Rectangle) {
                return (Rectangle)childNode;
            }
        }

        return null;
    }

    // Initialize timer
    private void initializeTime(long time) {
        timeStart = time;
        gameDone = false;
        timer = new Timer();
        timer.schedule(new printTime(),0 , 1000);
    }

    // On timer tick, update timer in UI
    class printTime extends TimerTask {
        @Override
        public void run() {
            String toPrint;
            if(!gameDone){
                toPrint = (((System.currentTimeMillis()-timeStart)/1000)+"");
            }else{
                toPrint = (((timeEnd-timeStart)/1000)+"");
                timer = null;
            }
            if(toPrint.length() > 3){
                toPrint = "999";
            }else {
                toPrint = "0".repeat(3 - toPrint.length()) + toPrint;
            }
            timerFelt.setText(toPrint);
        }
    }
}
