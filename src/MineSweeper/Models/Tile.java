package MineSweeper.Models;

public class Tile implements Cloneable{
	private boolean show = false;
	private int num;
	private boolean flag = false;

	//Tile constructor
	Tile(boolean t) {
		if (t) {
			num = -1;
		}
	}

	//returns the tiles num
	public int getNum() {
		return num;
	}

	//set the tiles num to the given
	public void setNum(int num) {
		this.num = num;
	}

	//set the given tile to shown
	public void show() { show = true; }

	//returns the tiles shown state
	public boolean getState() { return show; }

	//return the tiles flag state
	public boolean getFlag(){
		return flag;
	}

	//sets the tiles flag state to the given
	public void setFlag(boolean b) {
		this.flag = b;
	}

	public Tile clone() throws CloneNotSupportedException {
		super.clone();
		Tile re = new Tile(this.num < 0);
		re.show = this.show;
		re.flag = this.flag;
		re.num = num;
		return re;
	}
}
