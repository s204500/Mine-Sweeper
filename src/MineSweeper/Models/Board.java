package MineSweeper.Models;

import java.util.ArrayList;
import java.util.List;

public class Board implements Cloneable {
	private Tile[][] tiles;
	public boolean initialized = false;

	//Board Constructor
	public Board(int x, int y){
		tiles = new Tile[x][y];
	}

	//Initializes the board based on the staring click
	public void initialize(int b, String id) {
		initialized=true;
		//Marks the starting tiles
		int x=tiles.length, y=tiles[0].length;
		int[] coords = getCoords(id);
		int minX = Math.max(coords[0] - 1, 0);
		int maxX = Math.min(coords[0] + 1, x - 1);
		int minY = Math.max(coords[1] - 1, 0);
		int maxY = Math.min(coords[1] + 1, y - 1);
		for (int i = minX; i <= maxX; i++) {
			for (int j = minY; j <= maxY; j++) {
				tiles[i][j] = new Tile(false);
			}
		}
		//Sets Mine tiles
		for (int i = 0; i < b; i++) {
			int rX = (int) (Math.random() * x);
			int rY = (int) (Math.random() * y);
			if (tiles[rX][rY] == null) {
				tiles[rX][rY] = new Tile(true);
			} else {
				i--;
			}
		}
		//Sets Number and Blank tiles
		for (int i = 0; i < x; i++) {
			for (int j = 0; j < y; j++) {
				if (tiles[i][j] == null || tiles[i][j].getNum()!=-1) {
					tiles[i][j] = new Tile(false);
					tiles[i][j].setNum(getNeighbours(i, j));
				}

			}
		}
	}
	//Initializes the board based on data string
	//returns a list of the shown tiles
	public List<CoordCode> loadGame(String data) {
		initialized=true;

		String[] rows = data.split("\\.");
		List<CoordCode> list = new ArrayList<>();
		for (int i = 0; i<tiles.length; i++){
			String[] e = rows[i].split(":");
			for(int j=0; j<tiles[0].length; j++){
				String[] n = e[j].split(",");
				tiles[i][j] = new Tile(false);
				//Check if tile should be shown
				if(Integer.parseInt(n[0])==1){
					tiles[i][j].show();
					list.add(new CoordCode(i,j,Integer.parseInt(n[1])));
				}
				//Sets tiles num
				tiles[i][j].setNum(Integer.parseInt(n[1]));
				//Check if tile should have a flag
				if(Integer.parseInt(n[2])==1){
					tiles[i][j].setFlag(true);
				}
			}
		}
		return list;
	}

	//Get's coords from string and calls the recurring loop
	//returns the gotten list
	public List<CoordCode> showTiles(String id) {
		int[] coords = getCoords(id);
		return showTiles(coords[0],coords[1]);
	}

	public List<String> getMineIds() {
		List<String> coords = new ArrayList<>();
		for (int i = 0; i < tiles.length; i++) {
			for (int j = 0; j < tiles[0].length; j++) {
				if (tiles[i][j].getNum() < 0) {
					coords.add(i + "," + j);
				}
			}
		}

		return coords;
	}

	public boolean isGameWon(){
		for (Tile[] row: tiles) {
			for (Tile e : row) {
				if (!e.getState() && e.getNum() != -1) {
					return false;
				}
			}
		}
		return true;
	}

	public String toString() {
		StringBuilder string = new StringBuilder();
		for (Tile[] e : tiles) {
			for (Tile i : e) {
				string.append(i);
			}
			string.append("\n");
		}
		return string.toString();
	}

	//Get's coords from given id String
	private int[] getCoords(String id) {
		String[] args = id.split(",");

		int[] coords = new int[2];
		coords[0] = Integer.parseInt(args[0]);
		coords[1] = Integer.parseInt(args[1]);
		return coords;
	}

	//Finds ever mine tile around the given tile
	private int getNeighbours(int x, int y) {
		int sum = 0;
		int minX = Math.max(x - 1, 0);
		int maxX = Math.min(x + 1, tiles.length - 1);
		int minY = Math.max(y - 1, 0);
		int maxY = Math.min(y + 1, tiles[0].length - 1);
		for (int i = minX; i <= maxX; i++) {
			for (int j = minY; j <= maxY; j++) {
				if (tiles[i][j] != null && tiles[i][j].getNum()==-1) {
					sum++;
				}
			}
		}
		return sum;
	}

	//Recurring method that shows all tiles around a blank tile
	//returns a CoordCode list of all the shown tiles
	private List<CoordCode> showTiles(int x, int y) {
		int num = tiles[x][y].getNum();
		List<CoordCode> list = new ArrayList<>();
		//ignores tiles with flags on
		if(!tiles[x][y].getFlag()){
			list.add(new CoordCode(x,y,num));
			tiles[x][y].show();
		}
		if (num == 0) {
			int minX = Math.max(x - 1, 0);
			int maxX = Math.min(x + 1, tiles.length - 1);
			int minY = Math.max(y - 1, 0);
			int maxY = Math.min(y + 1, tiles[0].length - 1);
			for (int i = minX; i <= maxX; i++) {
				for (int j = minY; j <= maxY; j++) {
					if (!tiles[i][j].getState()) {
						list.addAll(showTiles(i, j));
					}
				}
			}
		}
		return list;
	}
	//set or removes flag on a the tiles form a given id.
	public boolean setFlag(String id) {
		int[] coords = getCoords(id);
		if (tiles[coords[0]][coords[1]].getFlag()) {
			tiles[coords[0]][coords[1]].setFlag(false);
			return false;
		} else {
			tiles[coords[0]][coords[1]].setFlag(true);
			return true;
		}
	}

	//returns the Tile[][] array
	public Tile[][] getBoardState(){
		return tiles;
	}

	public int getXDim(){
		return tiles.length;
	}

	public int getYDim(){
		return tiles[0].length;
	}

	//Denne klasse bruges til at lave en identisk kopi af objektet, så at når en AI løser den kan brugeren stadig få en der ikke er løst.
	public Board clone() throws CloneNotSupportedException {
		super.clone();
		Board re = new Board(this.getXDim(),this.getYDim());
		re.tiles = new Tile[this.getXDim()][this.getYDim()];
		for(int i = 0; i < this.getXDim(); i++){
			for(int j = 0; j < this.getYDim(); j++){
				re.tiles[i][j] = this.tiles[i][j].clone();
			}
		}

		re.initialized = this.initialized;
		return re;

	}
}
