package MineSweeper.Models;

public class CoordCode {
    public final int x, y, num;
    public CoordCode(int x, int y, int num){
        this.x=x;
        this.y=y;
        this.num=num;
    }

    public String toID(){
        return x + "," +y;
    }

    public String toString(){
        return toID() + "  " + num;
    }
}
