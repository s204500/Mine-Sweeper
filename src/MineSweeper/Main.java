package MineSweeper;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main extends Application {
	public static void main(String[] args) {
		launch(args);

	}

	@Override
	public void start(Stage stage) throws Exception {
		stage.setTitle("Minesweeper");

		FXMLLoader loader = new FXMLLoader(getClass().getResource("Views/minesweeper.fxml"));

		BorderPane root = loader.load();

		Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getResource("Views/style.css").toExternalForm());

		stage.setScene(scene);

		Controller controller = loader.getController();
		controller.setStage(stage);

		stage.setOnCloseRequest(windowEvent -> {
			Platform.exit();
			System.exit(0);
		});

		stage.show();
	}
}