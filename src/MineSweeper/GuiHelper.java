package MineSweeper;

import javafx.animation.FillTransition;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.util.Duration;


public class GuiHelper {
    public GuiHelper(double appWidth) {
        tileSize = (int)(appWidth / 40);
        numFont = Font.font("Verdana",FontWeight.BOLD,tileSize*0.8);
    }

    // Sizing
    public final int tileSize;

    // Tile colors
    public static final Color hiddenColor = Color.rgb(90, 90, 110);
    public static final Color revealedColor = Color.rgb(189, 189, 189);
    public static final Color hoverColor = Color.rgb(70, 70, 100);

    // Game indicator colors
    public static final Color lostFill = Color.rgb(220, 45, 0);
    public static final Color lostStroke = Color.rgb(255, 50, 0);
    public static final DropShadow lostShadow = new DropShadow(BlurType.GAUSSIAN, GuiHelper.lostStroke, 10, 0, 0, 0);
    public static final Color runningFill = Color.rgb(255, 190, 20);
    public static final Color runningStroke = Color.rgb(255, 220, 20);
    public static final DropShadow runningShadow = new DropShadow(BlurType.GAUSSIAN, GuiHelper.runningStroke, 10, 0, 0, 0);
    public static final Color wonFill = Color.rgb(130, 230, 50);
    public static final Color wonStroke = Color.rgb(150, 255, 100);
    public static final DropShadow wonShadow = new DropShadow(BlurType.GAUSSIAN, GuiHelper.wonStroke, 10, 0, 0, 0);

    public static void setLost(Circle gameIndicator) {
        gameIndicator.setFill(lostFill);
        gameIndicator.setStroke(lostStroke);
        gameIndicator.setEffect(lostShadow);
        gameIndicator.setUserData("lost");
    }
    public static void setRunning(Circle gameIndicator) {
        gameIndicator.setFill(runningFill);
        gameIndicator.setStroke(runningStroke);
        gameIndicator.setEffect(runningShadow);
        gameIndicator.setUserData("running");
    }
    public static void setWon(Circle gameIndicator) {
        gameIndicator.setFill(wonFill);
        gameIndicator.setStroke(wonStroke);
        gameIndicator.setEffect(wonShadow);
        gameIndicator.setUserData("won");
    }

    // Fonts
    public final Font numFont;

    public static final Color[] numColors = {
            Color.rgb(0,0,255),
            Color.rgb(0,123,0),
            Color.rgb(255,0,0),
            Color.rgb(0,0,123),
            Color.rgb(123,123,123),
            Color.rgb(0,123,123),
            Color.rgb(0,0,0),
            Color.rgb(123,123,123)
    };

    // Flag
    public static final Color flagColor = Color.INDIANRED;

    public Polygon getFlag() {
        Polygon flag = new Polygon();
        flag.getPoints().addAll(tileSize * 0.75, tileSize * 0.75, 0.0, (double)tileSize, 0.0, tileSize * 0.4);
        flag.setFill(GuiHelper.flagColor);
        return flag;
    }

    // Explosion
    public static final Color[] mineColors = {
            Color.rgb(255, 255, 255),
            Color.rgb(255, 250, 200),
            Color.rgb(250, 40, 40)
    };

    public static void explodeMine(Rectangle tile, int delay) {
        FillTransition animation1 = new FillTransition(Duration.millis(100), tile, null, mineColors[0]);
        FillTransition animation2 = new FillTransition(Duration.millis(400), tile, null, mineColors[1]);
        FillTransition animation3 = new FillTransition(Duration.millis(1000), tile, null, mineColors[2]);

        animation1.setOnFinished(e -> animation2.play());
        animation2.setOnFinished(e -> animation3.play());
        animation1.setDelay(Duration.millis(delay * 100));
        animation1.play();
    }

    // Success
    public static final Color successColorClear = wonStroke;
    public static final Color successColorMine = runningStroke;

    public static void victoryFlash(Rectangle tile, int delay, boolean hasMine) {
        FillTransition fillTransition;
        if (hasMine) {
            fillTransition = new FillTransition(Duration.millis(300), tile, null, GuiHelper.successColorMine);
        }
        else {
            fillTransition = new FillTransition(Duration.millis(300), tile, null, GuiHelper.successColorClear);
            fillTransition.setCycleCount(2);
            fillTransition.setAutoReverse(true);
        }
        fillTransition.setDelay(Duration.millis(delay * 50));
        fillTransition.play();
    }
}
